﻿insert into tempo_dim
select
   nextval('seq_tempo_dim'),
   case when cast(tmonth as int) = 1 then 'Jan'
	when cast(tmonth as int) = 2 then 'Fev'
	when cast(tmonth as int) = 3 then 'Mar'
        when cast(tmonth as int) = 4 then 'Abr'	
        when cast(tmonth as int) = 5 then 'Mai'
        when cast(tmonth as int) = 6 then 'Jun'
        when cast(tmonth as int) = 7 then 'Jul'
        when cast(tmonth as int) = 8 then 'Ago'
        when cast(tmonth as int) = 9 then 'Set'
        when cast(tmonth as int) = 10 then 'Out'
        when cast(tmonth as int) = 11 then 'Nov'
        when cast(tmonth as int) = 12 then 'Dez'
   end,
   cast(tyear as char(4)),
   tmonth,
   tyear
from (select cast(EXTRACT(MONTH from data) as int) as tmonth, cast(EXTRACT(YEAR from data) as int) as tyear
      from vendas
      group by 1,2
      order by 2) data_dim
order by 1;

//Criação da tabela tempo_dim (dimensão) usando um sequenciador como chave substituta.