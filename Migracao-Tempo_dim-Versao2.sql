﻿insert into tempo_dim
select
  cast(to_char(data,'yyyymmdd') as bigint) tempo_sk,
  EXTRACT(YEAR from data) ano,
  case
      when (EXTRACT(MONTH from data)) in (1,2,3) then 1
      when (EXTRACT(MONTH from data)) in (4,5,6) then 2
      when (EXTRACT(MONTH from data)) in (7,8,9) then 3
      when (EXTRACT(MONTH from data)) in (10,11,12) then 4
  end trimestre,
  case
      when (EXTRACT(MONTH from data)) in (1,2) then 1
      when (EXTRACT(MONTH from data)) in (3,4) then 2
      when (EXTRACT(MONTH from data)) in (5,6) then 3
      when (EXTRACT(MONTH from data)) in (7,8) then 4
      when (EXTRACT(MONTH from data)) in (9,10) then 5
      when (EXTRACT(MONTH from data)) in (11,12) then 6
  end bimestre,
  case 
      when (EXTRACT(MONTH from data)) = 1 then 'Janeiro'
      when (EXTRACT(MONTH from data)) = 2 then 'Fevereiro'
      when (EXTRACT(MONTH from data)) = 3 then 'Março'
      when (EXTRACT(MONTH from data)) = 4 then 'Abril'
      when (EXTRACT(MONTH from data)) = 5 then 'Maio'
      when (EXTRACT(MONTH from data)) = 6 then 'Junho'
      when (EXTRACT(MONTH from data)) = 7 then 'Julho'
      when (EXTRACT(MONTH from data)) = 8 then 'Agosto'
      when (EXTRACT(MONTH from data)) = 9 then 'Setembro'
      when (EXTRACT(MONTH from data)) = 10 then 'Outubro'
      when (EXTRACT(MONTH from data)) = 11 then 'Novembro'
      when (EXTRACT(MONTH from data)) = 12 then 'Dezembro'
  end nome_mes,    
  case 
      when (EXTRACT(dow from data)) = 0 then 'Domingo'
      when (EXTRACT(dow from data)) = 1 then 'Segunda'
      when (EXTRACT(dow from data)) = 2 then 'Terça'
      when (EXTRACT(dow from data)) = 3 then 'Quarta'
      when (EXTRACT(dow from data)) = 4 then 'Quinta'
      when (EXTRACT(dow from data)) = 5 then 'Sexta'
      when (EXTRACT(dow from data)) = 6 then 'Sábado'
  end nome_dia_semana
from (
  select '2013-01-01'::date + vl as data
  from generate_series(0,5000) vl 
) data_table
order by ano