﻿insert into filial_dim
select
   nextval('seq_filial_dim'),
   filial,
   estado
from (select filial_nome, filial_estado 
      from vendas
      group by filial_nome, filial_estado) data_dim 
order by 1;      

//Pode usar distinct também