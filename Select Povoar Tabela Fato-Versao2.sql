﻿insert into venda_fact
select
   filial_id,
   produto_id,
   tempo_id,
   soma
from (select f.filial_id, p.produto_id, t.tempo_id, sum(valor) as soma
from filial_dim f join vendas v on (v.filial = f.descricao)
     join produto_dim p on (v.produto = p.descricao)
     join tempo_dim t on ((cast(EXTRACT(MONTH from v.data) as int) = t.mes_valor) and (cast(EXTRACT(YEAR from v.data) as int) = t.ano_valor))
group by f.filial_id, p.produto_id, t.tempo_id) data_dim
     
     