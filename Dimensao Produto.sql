﻿insert into produto_dim
select
   nextval('seq_produto_dim'),
   produto,
   categoria
from (select produto_nome, produto_categoria 
      from vendas
      group by produto_nome, produto_categoria) data_dim 
order by 1;      

//Pode usar distinct também, pois cada linha tem que ser distinta nas tabelas dimensão.