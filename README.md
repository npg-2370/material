Este repositório contém os seguintes arquivos:

1) dataMart-1.2.0.jar (gerador de dados com driver postgresql) Para utilizar instalar o postgresql (banco de dados)
2) Arquivos com extensão ktr (Kettle). Usar o Pentaho Data Integration para abrir os arquivos.
3) Arquivo de job que utiliza os arquivos ktr do item anterior
4) Scripts SQL para a criação de tabelas dimensões e fato.